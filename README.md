# Docker Flask
A sample application intended to showcase daily techniques and practices in Python Flask/Celery development.

It takes the form of a simple CRUD application.  It's backed by a SQL database and it is protected by session-based authentication.

It uses Celery for asynchronous task execution.

## Prerequisites
- MySQL
- Docker
- Redis
- RabbitMQ

## Running the application
### Docker
Build container images using the two Dockerfiles and run them.

NB: Be sure to take the properties in core_app/.env-sample, replace them with appropriate values for your environment and add them to your Docker/Kubernetes environment.

### Visual Studio Code
Download to Visual Studio Code and run in the debugger.

NB: Rename core_app/.env-sample to core_app/.env and fill in the properties with appropriate values for your environment.

## Front-end Application Structure
### Home
A simple home page for the application.  No authentication.
### List Customers
List the customers in the underlying database.  Requires authentication.
### Update Customer
Opened from the List Customers page to display/update/delete individual records.
### New Customer
A form to create a new Customer record in the database.  Requires authentication.
### Celery
A page which runs a chain of tasks and displays a result.  Requires authentication.
### Intense
A page which carries out a CPU-intensive calculation and returns a result as JSON.  This can be used to test the Horizontal Pod Autoscaler in Kubernetes.  Requires authentication.
## Back-end application
### Save customer
Saves all changes to customer records.  Drops data into an asynchronous Celery task to distribute load.
### Delete customer
Delete an individual customer, based on the id passed in from the **Update Customer** page.
### Celery
There are three back-end Celery tasks:
- Add
    - Add y to x
- Subtract
    - Subtract y from x 
- Do Save Customer
    - Insert the supplied payload into the SQL database

If an error occurs in a Celery task, Celery will send a message to Azure Service Bus, which can be procesed with the likes of Power Automate etc.

## Unit Testing
There are separate Python unittest test cases for the Flask and the Celery components.  These are defined in **core_app\test_app.py** file

A dedicated testing database is created on setUp and dropped on tearDown.  You can specify a name for this by altering the SQL_DB property in the .env file.

When using the Flask test suite, you will need to run a Celery worker to process the celery tasks.
## Docker
There are separate Dockerfiles to create dedicated Flask and Celery container images.

The Docker images have been tested on Azure Kubernetes Service