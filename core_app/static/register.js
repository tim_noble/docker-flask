/**
 * Is the email a valid RFC email?
 * 
 * @param {*} value
 * @return bool
 */
function emailIsValid(value) {

	if (value.match(/^(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/i)) {
		return true;
	}

	return false;

}

/**
 * Is the password valid?
 * 
 * @param {*} value
 * @return bool
 */
function passwordIsValid(value) {

	if (value.match(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()_+])[0-9a-zA-Z!@#$%^&*()_+]{8,}$/g)) {
		return true;
	}

	return false;

}

/**
 * Do the two password fields match?
 * 
 * @param {*} value
 * @return bool
 */
function passwordsMatch() {

	var password = document.getElementById('password')
	var confirmPassword = document.getElementById('confirm-password')

	if (password.value == confirmPassword.value) {
		return true;
	}

	return false;

}



/**
 * Does the supplied element pass its custom validation?
 *  
 * @param {*} element 
 * @return bool
 */
function passesCustomValidation(element) {

	switch (element.id) {
		case "username":
			return emailIsValid(element.value);
			break;
		case "password":
			return passwordIsValid(element.value);
			break;
		case "confirm-password":
			return passwordsMatch(element.value);
			break;
	}

}