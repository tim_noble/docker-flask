/**
 * Check that the src element of the supplied event passes custom validation
 * 
 * @param {Event} event 
 */
function customValidityCheck(event) {
	
	//get the HTML element from the event
	var field = event.srcElement

	if (!passesCustomValidation(field)) {
		field.setCustomValidity("Invalid")
	} else {
		field.setCustomValidity("")
	}
}

/**
 * Initialise field validation on page load
 */
(function() {
	'use strict';
	window.addEventListener('load', function() {

		//get all fields marked for custom validation
		var customValidationFields = document.getElementsByClassName('custom-validation');

		//test custom field's validity on change or blur
		var validation = Array.prototype.filter.call(customValidationFields, function(field) {
			field.addEventListener('blur', customValidityCheck, false);
			field.addEventListener('change', customValidityCheck, false);
		});

		// Fetch all the forms we want to apply custom Bootstrap validation styles to
		var forms = document.getElementsByClassName('needs-validation');
		// Loop over them and prevent submission
		var validation = Array.prototype.filter.call(forms, function(form) {
		form.addEventListener('submit', function(event) {
			if (form.checkValidity() === false) {
			event.preventDefault();
			event.stopPropagation();
			}
			form.classList.add('was-validated');
		}, false);
		});
	}, false);
})();