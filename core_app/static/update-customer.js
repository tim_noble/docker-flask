/**
 * Is the postcode a UK postcode?
 * 
 * @param {*} element
 * @return bool
 */
function postCodeIsValid(value) {

	if (value.match(/^([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9][A-Za-z]?))))\s?[0-9][A-Za-z]{2})$/i)) {
		return true;
	}

	return false;

}

/**
 * Is the phone number a UK phone number?
 * 
 * @param {*} value
 * @return bool
 */
function phoneNumberIsValid(value) {
	
	if (value.match(/^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$/i)) {
		return true;
	}

	return false;

}

/**
 * Does the supplied element pass its custom validation?
 *  
 * @param {*} element 
 * @return bool
 */
function passesCustomValidation(element) {

	switch (element.id) {
		case "post_code":
			return postCodeIsValid(element.value);
			break;
		default:
			return phoneNumberIsValid(element.value);
			break;
	}

}