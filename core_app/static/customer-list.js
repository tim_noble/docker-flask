/**
 * Initialise the customer list as a jQuery datatable
 */
$(document).ready(function() {

	$('table.data-table').DataTable({
		"order": [[ 3, "desc" ]],
		"ajax":{
			url: "customers",
			type: "POST"
		},
		"columns":[
			{data:"name"},
			{data:"post_code"},
			{data:"country_code"},
			{data:"updated", "render": $.fn.dataTable.render.moment('YYYY-MM-DDTHH:mm:ss', 'DD-MMM-YYYY HH:mm:ssZ' )},
			{data:"id", "render": function ( data, type, row, meta ) {
					return '<a class="btn btn-outline-secondary" href="customer/'+data+'"><span class="oi oi-eye"></span></a>';
				}
			}
		]
	});
});