from core_app.format import FormatHelper
from datetime import datetime
from sqlalchemy import Column, Integer, String, DateTime, Text, Sequence
from sqlalchemy.ext.declarative import declarative_base
from werkzeug.security import check_password_hash

Base = declarative_base()

class User(Base):

	__tablename__ = 'user'

	id = Column(Integer, Sequence('user_id_seq'), primary_key=True, nullable=False)
	username = Column(String(50))
	password = Column(Text)

	def __repr__(self):
		return "<User (id={0}, username={1})>".format(self.id, self.username)

	def password_matches(self, password):
		return check_password_hash(self.password, password)


class Customer(Base):

	__tablename__ = 'customer'

	id = Column(Integer, Sequence('customer_id_seq'), primary_key=True, nullable=False)
	name = Column(String(50))
	address_one = Column(String(50))
	post_code = Column(String(20))
	phone_number = Column(String(20))
	country_code = Column(String(3))
	created = Column(DateTime, default=datetime.utcnow)
	updated = Column(DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)

	def __repr__(self):
		return "<Customer(id='%d', name='%s', address_one='%s', post_code='%s')>" % (self.name, self.address_one, self.post_code)