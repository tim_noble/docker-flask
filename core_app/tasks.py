from celery import Celery
from celery import Task as CeleryTask
from celery.utils.log import get_task_logger
from core_app.client import AzureServiceBusTopicClient
from core_app.config import CustomConfigParser
from core_app.dao import CustomerDao
from core_app.database import SqlHelper, session_scope
from core_app.schema import CustomerSchema
import traceback, os, copy

app = Celery('core_app.tasks')
app.conf.result_backend_transport_options = { 'master_name': "mymaster" if "REDIS_MASTER_NAME" not in os.environ else os.environ["REDIS_MASTER_NAME"]}
app.conf.update(CustomConfigParser.make_flask_like_config())
logger = get_task_logger(__name__)

class MyTask(CeleryTask):

	def on_failure(self, exc, task_id, args, kwargs, einfo):

		service_bus_client = AzureServiceBusTopicClient()

		service_bus_client.publish({"error": traceback.format_exception(type(exc), exc, exc.__traceback__)})

		super(MyTask, self).on_failure(exc, task_id, args, kwargs, einfo)

@app.task(base=MyTask, bind=True)
def add(self, x, y):
	"""Add y to x

		:param (int) x
		:param (int) y
	"""
	logger.info("Do the add")
	return x + y

@app.task(base=MyTask, bind=True)
def subtract(self, x, y):
	"""Subtract y from x

		:param (int) x
		:param (int) y
	"""
	logger.info("Do the subtract")
	return x - y

@app.task(base=MyTask, bind=True)
def do_save_customer(self, payload):
	"""Save customer to the database

		:param (dict) payload
			The data to be saved to the database
	"""
	logger.info("Do the save customer")
	customer = None

	# wrap the operation in a database transaction
	with session_scope(SqlHelper.build_session()) as db_session:

		# use our dao to carry out the update operation
		dao = CustomerDao(db_session)
		db_model = dao.update(payload)

		# flush the session to reload the changes from the database
		db_session.flush()

		# copy the db_model to a new variable for use once the db_session is closed
		customer = copy.copy(db_model)

	# use the CustomerSchema to convert the model to a JSON-serializable form
	schema = CustomerSchema()
	return schema.dump(customer)