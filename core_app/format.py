import re, ast
from dateutil import parser
from uuid import uuid4

class FormatHelper:

	@staticmethod
	def generate_uuid():
		"""Generate a v4 uuid"""
		return str(uuid4())

	def format_value(self, value):
		"""	Convert string value to Python type

			:param (str) value
				The value to convert
		"""

		if re.search(r"^(([0-9])|([0-2][0-9])|([3][0-1]))\-(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\-\d{4}$",value):
			new_value = dateutil_parser.parse(value)
		elif re.search(r"^([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))$",value):
			new_value = dateutil_parser.parse(value)
		elif re.search(r"^\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d(\.\d+)?([+-][0-2]\d:[0-5]\d|Z)$", value):
			new_value = dateutil_parser.parse(value)
		elif re.search(r"^(\d+.)\d+$", value):
			new_value = float(value)
		elif re.search(r"^\d+$", value):
			new_value = int(value)
		elif re.search(r"^\{(.*?)\}$", value):
			new_value = ast.literal_eval(value)
		else:
			new_value = value

		return new_value