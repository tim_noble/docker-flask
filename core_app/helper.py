from flask import current_app, session, redirect, request
from functools import wraps

class SecurityHelper:

	def auth_required(f):
		"""Provide an auth_required decorator for Flask routes
		
			:param (function) f
		"""
		@wraps(f)
		def decorated(*args, **kwargs):
			"""If the current Flask session does not contain a user, redirect to login"""
			if current_app.config["USER_SESSION_KEY"] not in session:
				
				# add the url requeste to the session
				session[current_app.config["LOGIN_REDIRECT_SESSION_KEY"]] = request.url

				# redirect to login
				return redirect(current_app.config["LOGIN_URI"])

			return f(*args, **kwargs)
		return decorated

	@staticmethod
	def raise_for_login_keys(keys):

		"""Raise a KeyError if the vital keys are missing

			:param (list) keys
				The keys to check
		"""

		# we need both keys in the POST data
		if "username" in keys:
			if "password" in keys:
				pass
			else:
				raise KeyError("Missing password")
		else:
			raise KeyError("Missing username")