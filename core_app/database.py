from contextlib import contextmanager
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import os

class SqlHelper:

	@staticmethod
	def build_conn_str(use_mysql_db=False):
		"""Build a SQL connection string from environment vars"""
		return 'mysql+pymysql://{0}:{1}@{2}:{3}/{4}{5}'.format(
			os.environ['SQL_USER'],
			os.environ['SQL_PASSWORD'],
			os.environ['SQL_HOST'],
			os.environ["SQL_PORT"],
			"" if use_mysql_db else os.environ['SQL_DB'],
			os.environ['SQL_QUERYSTRING']
		)

	@staticmethod
	def build_engine(use_mysql_db=False):
		"""Build a SQLAlchemy engine"""
		# new sqlalchemy engine
		return create_engine(SqlHelper.build_conn_str(use_mysql_db))
	
	@staticmethod
	def build_session():
		"""Build a SQLAlchemy session"""
		# build a session with the engine we created above
		return sessionmaker(bind=SqlHelper.build_engine())()

# new Flask SQLAlchemy
db = SQLAlchemy()

@contextmanager
def session_scope(session=None):
	"""Provide a transactional scope around a series of database operations."""

	# if session is not passed in, use the session from the Flask SQLAlchemy object
	session = db.session if session is None else session
	
	try:
		yield session
		session.commit()
	except:
		session.rollback()
		raise
	finally:
		session.close()