from core_app.app import create_app

"""
	Create and run the Flask app if this file is called directly
	Create and return Flask app to WSGI server if imported
"""

if __name__ == '__main__':
	create_app = create_app()
	create_app.run()
else:
	wsgi_app = create_app()