class LoginException(Exception):
	pass

class UserRegisterException(Exception):
	pass

class RecordNotFoundException(Exception):
	pass