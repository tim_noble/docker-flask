import os, pathlib
from configparser import RawConfigParser
from core_app.format import FormatHelper

class CustomConfigParser:

	DEFAULT_SETTINGS_ENV_VAR = "APP_CONFIG"

	@staticmethod
	def make_flask_like_config():

		"""Convert a ini file on disk to a flask-like config

			Returns:

				dict
		"""

		file_content = '[to_be_ignored]\n'

		with open(os.path.join(os.path.dirname(__file__), os.environ[CustomConfigParser.DEFAULT_SETTINGS_ENV_VAR]), mode='r') as config_file:
			file_content += config_file.read()

		# new raw config parser
		config_parser = RawConfigParser(strict=False)

		# stop config parser switching key to lowercase
		config_parser.optionxform = str

		# read file_content to configparser
		config_parser.read_string(file_content)

		# blank dict
		my_dict = {}

		# format helper
		formatter = FormatHelper()

		# loop through the sections in the ini file
		for s in config_parser.sections():

			# dict comprehension to convert the section's items to a dict and then loop through the key/value pairs of the dict
			my_dict.update({k:formatter.format_value(v.replace("\"", "")) for k,v in dict(config_parser.items(s)).items()})

		# return dictionary
		return my_dict