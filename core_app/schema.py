from core_app.model import Customer, User
from marshmallow import Schema, fields, post_load

class CustomerSchema(Schema):

	id = fields.Str(allow_none=True)
	name=fields.Str()
	address_one = fields.Str()
	post_code = fields.Str()
	country_code=fields.Str()
	phone_number=fields.Str()
	updated = fields.DateTime()
	created = fields.DateTime()

	@post_load
	def make_object(self, data, **kwargs):
		"""Convert the supplied data into a Customer model

			:param (dict) data
				The data to convert
		"""
		return Customer(**data)

class UserSchema(Schema):

	id = fields.Str(allow_none=True)
	username=fields.Str()
	password=fields.Str()

	@post_load
	def make_object(self, data, **kwargs):
		"""Convert the supplied data into a User model

			:param (dict) data
				The data to convert
		"""
		return User(**data)