from celery import chain
from core_app.app import create_app
from core_app.database import SqlHelper, session_scope
from core_app.model import Customer
from core_app.schema import CustomerSchema
from core_app.tasks import *
from datetime import datetime
from flask import url_for
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import MetaData, Table
from unittest.mock import patch
import unittest, json, time, os

class CustomTestCase(unittest.TestCase):

	EFFECTIVE_USER = "t_noble77@hotmail.com"

	def setUp(self):

		"""
			Create the database
			Add the tables to the database
			Populate the tables
		"""
		my_app = create_app()
		self.client = my_app.test_client()

		# create a new database for testing
		self.engine = SqlHelper.build_engine(use_mysql_db=True)
		self.engine.execute("CREATE DATABASE {}".format(os.environ["SQL_DB"]))

		# create a test customer for use throughout
		self.test_customers = [
			Customer(name="Test Customer 1", address_one="Test Address 1", post_code="SY23 3SR", phone_number="+44 7971 970778", country_code="GBR", created=datetime.utcnow(), updated=datetime.utcnow()),
			Customer(name="Test Customer 2", address_one="Test Address 2", post_code="SY24 3SR", phone_number="+44 7971 970779", country_code="GBR", created=datetime.utcnow(), updated=datetime.utcnow()),
			Customer(name="Test Customer 3", address_one="Test Address 3", post_code="44060", phone_number="+1 212 334 3456", country_code="USA", created=datetime.utcnow(), updated=datetime.utcnow())
		]

		# build a new engine to use our testing table
		self.engine = SqlHelper.build_engine()

		# new instance of Customer model
		customer = Customer()

		# use the customer model to create all the necessary tables in the engine
		customer.metadata.create_all(self.engine)

		# add test data to the database
		with session_scope(SqlHelper.build_session()) as db_session:
			for c in self.test_customers:
				db_session.add(c)

	def tearDown(self):
		"""Drop the database"""

		# drop the database
		self.engine.execute("DROP DATABASE {}".format(os.environ["SQL_DB"]))

class FlaskTestCase(CustomTestCase):

	def test_login(self, method='GET', url="/login"):

		"""
			Test that the login view is returned

				:param (str) method
					The http method to use
				:param (str) url
					The url to target
		"""

		if method == 'GET':
			result = self.client.get(url, follow_redirects=True)
		else:
			result = self.client.post(url, follow_redirects=True)

		self.assertEqual(result.status_code, 200)
		self.assertIn(b'Username', result.data)

	def test_logout(self):

		"""Test the /logout endpoint
		
			Get the URL without the effective_user session variable and check that the login screen is returned
			Add the effective_user session variable, try again and check that the Login template is returned
		"""

		url = "/logout"

		self.test_login(url=url)

		# now add an effective_user element to the session to simulate a login
		with self.client.session_transaction() as sess:
			sess["effective_user"] = self.EFFECTIVE_USER

		result = self.client.get(url, follow_redirects=True)
		self.assertIn(b"Username", result.data)

	def test_home(self):

		"""Get the home page.  No login necessary"""

		result = self.client.get("/")
		self.assertEqual(result.status_code, 200)

	def test_celery(self):

		"""Test the /celery endpoint
		
			Get the URL without the effective_user session variable and check that the login screen is returned
			Add the effective_user session variable, try again and check that the Intense JSON is returned
		"""

		# url for use in test case
		url = "/celery"

		# test the login
		self.test_login()

		# now add an effective_user element to the session to simulate a login
		with self.client.session_transaction() as sess:
			sess["effective_user"] = self.EFFECTIVE_USER

		# you should get a table of results with Postcode as one of the column headings
		result = self.client.get("/celery", follow_redirects=True)
		self.assertEqual(result.status_code, 200)
		self.assertIn(b'The chain result is 1', result.data)

	def test_intense(self):

		"""Test the /intense endpoint
		
			Get the URL without the effective_user session variable and check that the login screen is returned
			Add the effective_user session variable, try again and check that JSON is returned
		"""

		# url for use in test case
		url = "/intense"

		# check initial redirection to log in
		self.test_login(url=url)

		# now add an effective_user to the session to simulate a logged in user
		with self.client.session_transaction() as sess:
			sess["effective_user"] = self.EFFECTIVE_USER

		# did the result come back OK?
		result = self.client.get(url, follow_redirects=True)
		self.assertEqual(result.status_code, 200)

		# is there JSON in the result?
		json_response = json.loads(result.get_data(as_text=True))
		self.assertIn("output", json_response)
		self.assertIn("that was intense!", json_response["output"])

	def test_customer_list(self):

		"""Test the "/customers" endpoint

			Get the URL without the effective_user session variable and check that the login screen is returned
			Add the effective_user session variable, try again and check that the Customer list is returned
		"""

		# url for use in this test
		url = "/customers"

		# check initial redirection to log in
		self.test_login(url=url)

		# now add an effective_user to the session to simulate a logged in user
		with self.client.session_transaction() as sess:
			sess["effective_user"] = self.EFFECTIVE_USER

		# you should get a table of results with Postcode as one of the column headings
		result = self.client.get(url, follow_redirects=True)
		self.assertEqual(result.status_code, 200)
		self.assertIn(b'Postcode', result.data)

	def test_save_customer(self):

		"""Test the "/save-customer" endpoint

			Get the URL without the effective_user session variable and check that the login screen is returned
			Add the effective_user session variable, try again and check that the save is successful

			NB: You will need a Celery worker running for this test to write to the database
		"""

		url = "/save-customer"
		
		# use our schema to convert the customer model to a celery payload
		schema = CustomerSchema()

		# check initial redirection to log in
		self.test_login(method='POST', url=url)

		# now add an effective_user to the session to simulate a logged in user
		with self.client.session_transaction() as sess:
			sess["effective_user"] = self.EFFECTIVE_USER

		# post the new customer to the endpoint
		result = self.client.post(
			url,
			data=schema.dump(Customer(name="Test API Customer", address_one="Test Address API", post_code="SY23 3SR", phone_number="+44 7971 970788", country_code="GBR", created=datetime.utcnow(), updated=datetime.utcnow())),
			follow_redirects=True
		)

		# the API should redirect to the Success page
		self.assertEqual(result.status_code, 200)
		self.assertIn(b"Success", result.data)

	def test_register(self):
		"""Test that you can register a user"""

		# send a user account payload
		result = self.client.post(
			"/register",
			data={"username":"tim.noble77@gmail.com", "password":"aA1!aaaa"},
			follow_redirects=True
		)

		self.assertEqual(result.status_code, 200)

	def test_success(self):

		"""Get the Success page.  No login necessary"""

		result = self.client.get("/success")
		self.assertEqual(result.status_code, 200)
		self.assertIn(b'Success', result.data)

class CeleryTestCase(CustomTestCase):

	def test_chain(self):
		"""Test Celery's chain primitive """

		# call the chain and wait for a result
		res = chain(add.s(1,2), subtract.s(2)).apply().get()

		self.assertEqual(res, 1)

	def test_add(self):
		"""Test the add task"""

		# Call the task and wait for a result
		res = add.s(1,2).apply().get()
		
		self.assertEqual(res, 3)

	def test_subtract(self):
		"""Test the subtract task"""

		# Call the task and wait for a result
		res = subtract.s(3,2).apply().get()

		self.assertEqual(res, 1)

	def test_do_save_customer(self):
		"""Test the do_save_customer task"""

		# new customer model
		customer = Customer(name="Test Customer", address_one="Test Address 1", post_code="SY23 3SR", phone_number="+44 7971 970778", country_code="GBR", created=datetime.utcnow(), updated=datetime.utcnow())
		
		# use our schema to convert the customer model to a celery payload
		schema = CustomerSchema()

		# run the celery task and wait for a result
		res = do_save_customer.s(schema.dump(customer)).apply().get()

		# can we load the response back into a Customer object
		schema = CustomerSchema()
		model = schema.load(res)

		# check that a customer model has come back
		self.assertIsInstance(model, Customer)