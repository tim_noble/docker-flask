from core_app.exception import RecordNotFoundException
from core_app.model import Customer, User
from core_app.schema import CustomerSchema, UserSchema

class Dao:

	def __init__(self, db_session):

		self.db_session = db_session

	def get_one_by_id(self, id):
		"""Return a model that matches the supplied id

			:param (sqlalchemy.orm.session.Session) db_session
				The active session
			:param (int) id
				The id of the model you wish to query
		"""
		return self.db_session.query(self.MODEL).filter(self.MODEL.id == id).first()

	def get_all(self):
		"""Return all models

			:param (sqlalchemy.orm.session.Session) db_session
				The active session
		"""

		return self.db_session.query(self.MODEL).all()

	def __update_attrs(self, payload, db_model, create=False):
		"""Loop through the keys and values in the payload dict and update the model with the value if it has an attr matching the key

			:param (dict) payload
				The dict containing the values to update
			:param (sqlalchemy.ext.declarative.declarative_base) db_model
				The db model to update
		"""
		for attr, value in payload.items():
			if hasattr(db_model, attr):
				setattr(db_model, attr, value)

	def update(self, payload):
		"""Insert or update the database record with the values in the supplied model

			:param (dict) payload
				The Customer data you want to save to the database
		"""

		if "id" in payload.keys() and payload["id"] is not None:

			db_model = self.get_one_by_id(payload["id"])

			if db_model is not None:
				self.__update_attrs(payload, db_model)
			else:
				raise RecordNotFoundException("Unable to load record with id {}".format(payload["id"]))

		else:
			schema = self.SCHEMA(unknown='EXCLUDE')
			db_model = schema.load(payload)
			self.db_session.add(db_model)

		return db_model

	def delete(self, id):
		"""Delete customer with id matching the supplied id

			:param (int) id
		"""
		return self.db_session.query(self.MODEL).filter(self.MODEL.id == id).delete()

class UserDao(Dao):

	MODEL = User
	SCHEMA = UserSchema

	def get_one_by_username(self, username):
		"""Return User model that matches the supplied username

			:param (sqlalchemy.orm.session.Session) db_session
				The active session
			:param (str) username
				The username of the model you wish to query
		"""
		return self.db_session.query(self.MODEL).filter(self.MODEL.username == username).first()


class CustomerDao(Dao):

	MODEL = Customer
	SCHEMA = CustomerSchema