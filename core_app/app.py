from core_app.blueprint import bp
from core_app.config import CustomConfigParser
from core_app.database import SqlHelper, db
from flask import Flask
from flask_session import Session
from redis import Redis
import os

def create_app(*args, **kwargs):
	"""Flask application factory function"""

	# initialise a Flask application
	app = Flask(__name__)

	# load the Flask config referred to in the env var
	if (CustomConfigParser.DEFAULT_SETTINGS_ENV_VAR not in os.environ):
		raise RuntimeError("Please export the {} environment variable".format(CustomConfigParser.DEFAULT_SETTINGS_ENV_VAR))

	app.config.from_envvar(CustomConfigParser.DEFAULT_SETTINGS_ENV_VAR)

	# these two config properties allow db (imported above) to connect to our database throughout the app
	app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
	app.config['SQLALCHEMY_DATABASE_URI'] = SqlHelper.build_conn_str()

	# start a session with Redis as the back-end
	session_redis = Redis.from_url(os.environ["SESSION_REDIS_URL"])
	app.config.update(
		SESSION_REDIS=session_redis
	)

	# register the application blueprints
	app.register_blueprint(bp)

	# init the database on the app
	db.init_app(app)

	# init the session on the app
	sess = Session()
	sess.init_app(app)

	return app