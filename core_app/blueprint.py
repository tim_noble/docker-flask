import os, dateutil.parser, base64, json
from copy import copy
from flask import current_app, Blueprint, jsonify, request, render_template, url_for, redirect, session
from celery import chain
from core_app.tasks import add, subtract, do_save_customer
from core_app.dao import CustomerDao, UserDao
from core_app.database import session_scope
from core_app.exception import LoginException, UserRegisterException
from core_app.helper import SecurityHelper
from core_app.schema import CustomerSchema
from math import sqrt
from werkzeug.security import generate_password_hash, check_password_hash

# new Blueprint
bp = Blueprint('default', __name__)

@bp.route('/', methods=['GET'], strict_slashes=False)
def home():
	"""Return the site's home page"""
	return render_template('default.html', title='Home')

@bp.route('/customer', methods=['GET'], strict_slashes=False, defaults={"id":-1})
@bp.route('/customer/<int:id>', methods=['GET'], strict_slashes=False)
@SecurityHelper.auth_required
def update_customer(id):
	"""Return the customer update page"""

	customer = None

	if id > -1:

		# start a new session with the database
		with session_scope() as db_session:

			# new instance of our customer dao
			dao = CustomerDao(db_session)
			
			# try to get the customer with the supplied id
			db_customer = dao.get_one_by_id(id)

			if db_customer is not None:
				customer = copy(db_customer)

	# render the update
	return render_template('update-customer.html', title='Update Customer', customer=customer)

@bp.route('/customers', methods=['GET'], strict_slashes=False)
@SecurityHelper.auth_required
def list_customers():
	"""Return the list of customers"""
	return render_template('customer-list.html', title='Customer List')

@bp.route('/login', methods=['POST'], strict_slashes=False)
def login():

	"""
		Select the user from the record matching the posted username
		Compare the posted password to the stored hash
	"""

	# get the form as a dict
	submission = request.form.to_dict(flat=True)

	# raise errors if vital keys are missing
	SecurityHelper.raise_for_login_keys(submission.keys())
			
	# start a database transaction
	with session_scope() as db_session:

		# user our dao to get the user by username
		dao = UserDao(db_session)
		user = dao.get_one_by_username(submission["username"])

		if user is not None:

			# does the supplied password match the hashed password?
			if user.password_matches(submission["password"]):

				# add the user to the session
				session[current_app.config["USER_SESSION_KEY"]] = user.username

				# does the session contain a redirect_url?
				if current_app.config["LOGIN_REDIRECT_SESSION_KEY"] in session.keys():

					# pull the redirect_url from the session and store it in a var
					redirect_url = session[current_app.config["LOGIN_REDIRECT_SESSION_KEY"]]

					# delete the redirect_url from session
					del session[current_app.config["LOGIN_REDIRECT_SESSION_KEY"]]
				else:
					redirect_url = "/"

				# redirect the user to the ur;l
				return redirect(redirect_url)
			else:
				raise LoginException("Invalid password")
		else:
			raise LoginException("User not recognised")

@bp.route('/logout', methods=['POST', 'GET'], strict_slashes=False)
@SecurityHelper.auth_required
def logout():
	"""Remove the user from the session and redirect to login"""
	if current_app.config["USER_SESSION_KEY"] in session.keys():
		del session[current_app.config["USER_SESSION_KEY"]]
		return redirect(current_app.config["LOGIN_URI"])
	else:
		raise KeyError("No user in session")

@bp.route('/login', methods=['GET'], strict_slashes=False)
def show_login():
	"""Return the login page"""
	return render_template('login.html', title='Log In')

@bp.route('/register', methods=['POST'], strict_slashes=False)
def register():
	"""Register the user"""
	
	# get the form as a dict
	submission = request.form.to_dict(flat=True)

	# raise errors if vital keys are missing
	SecurityHelper.raise_for_login_keys(submission.keys())

	# database transaction
	with session_scope() as db_session:

		# try to load an existing user with this username
		dao = UserDao(db_session)

		# get the user by username
		user = dao.get_one_by_username(submission["username"])

		# no user exists, so register
		if user is None:

			# hash the password
			submission["password"] = generate_password_hash(submission["password"])

			# insert to the databsae
			dao.update(submission)

			# redirect to success
			return redirect(url_for("default.show_success"))

		else:
			raise UserRegisterException("{} is already registered".format(user.username))

@bp.route('/register', methods=['GET'], strict_slashes=False)
def show_register():
	"""Return the register page"""
	return render_template('register.html', title='Register')

@bp.route('/customers', methods=['POST'], strict_slashes=False)
@SecurityHelper.auth_required
def query_all_customers():
	"""Load all customers from the db and return as JSON"""
	with session_scope() as db_session:

		dao = CustomerDao(db_session)
		models = dao.get_all()
		schema = CustomerSchema(many=True)

		return jsonify(data=schema.dump(models))

@bp.route('/save-customer', methods=['POST'], strict_slashes=False)
@SecurityHelper.auth_required
def save_customer():
	"""Save the submitted customer to the database"""

	# get the form as a dict
	submission = request.form.to_dict(flat=True)

	# pass the payload to the Celery task and run it
	do_save_customer.s(submission).apply_async()

	# redirect the user to the success page
	return redirect(url_for('default.show_success'))

@bp.route('/delete-customer/<int:id>', methods=['GET'], strict_slashes=False)
@SecurityHelper.auth_required
def delete_customer(id):
	"""Delete customer with id matching the supplied id

		:param (int) id
	"""
	# wrap this operation in a transaction
	with session_scope() as db_session:

		dao = CustomerDao(db_session)

		dao.delete(id)

	# redirect the user to the success page
	return redirect(url_for('default.show_success'))

@bp.route('/success', methods=['GET'], strict_slashes=False)
def show_success():
	"""Render the success page"""
	return render_template('success.html', title='Success')

@bp.route('/intense', methods=['GET'], strict_slashes=False)
@SecurityHelper.auth_required
def intense():
	"""Carry out CPU intensive calculation (for use with Kubernetes Horizontal Pod Autoscaler test)"""
	x = 0.0001

	for i in range(0,1000000):
		x += sqrt(x)

	return jsonify(output="{} -> that was intense!".format(str(x)))

@bp.route('/celery', methods=['GET'], strict_slashes=False)
@SecurityHelper.auth_required
def celery():
	"""Run the add and subtract tasks in a chain, return the outcome in an html template"""

	# construct and run the chain
	result = chain(
		add.s(2,3),
		subtract.s(4)
	)()

	# wait for the result
	result = result.get()

	# render the template passing a title and the outcome as arguments
	return render_template('default.html', title='Celery', output="The chain result is {}".format(result))

@bp.route('/favicon.ico', methods=['GET'], strict_slashes=False)
def favicon():
	return url_for('static', filename='favicon.ico')