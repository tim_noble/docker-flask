from azure.servicebus.control_client import ServiceBusService, Message as ControlClientMessage, Topic, Rule, DEFAULT_RULE_NAME
import os, json

class AzureServiceBusTopicClient:

	def __init__(self):

		self.bus_service = ServiceBusService(service_namespace=os.environ["AZURE_SERVICEBUS_NAMESPACE"], shared_access_key_name=os.environ["AZURE_SERVICEBUS_SAS_KEY_NAME"], shared_access_key_value=os.environ["AZURE_SERVICEBUS_SAS_KEY_VALUE"])

	def publish(self, dict_payload):

		"""Send a message to an Azure Service Bus topic

			:param (dict) dict_payload
				The message to send to Azure Service Bus
		"""

		if dict_payload is not None:

			# here's a new message
			msg = ControlClientMessage(json.dumps(dict_payload).encode())

			# send the message to the topic
			self.bus_service.send_topic_message(os.environ["AZURE_SERVICEBUS_TOPIC"], msg)

		else:
			raise ValueError("dict_payload is None")